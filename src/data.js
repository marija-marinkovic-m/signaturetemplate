import { parsePhoneNumberFromString } from "libphonenumber-js";
import banners from "./banners";

const data = {
  YOUR_EMAIL: "bdms@tourradar.com",
  YOUR_NAME: "",
  YOUR_TITLE: "Business Development Team",
  BANNER_SRC: banners[2].src,
  PHONE_NUMBER: "",
  BANNER: function() {
    return function() {
      return data.BANNER_SRC
        ? `
            <p
              dir="ltr"
              style="line-height:1.38;margin-top:16pt;margin-bottom:0pt">
              <span><a
                  href="https://www.tourradar.com"
                  target="_blank"><span style="font-size:11pt;font-family:Arial;color:rgb(17,85,204);background-color:transparent;vertical-align:baseline;white-space:pre-wrap"><img src="${data.BANNER_SRC}" width="560" height="128" style="border:none" class="CToWUd"/></span></a></span><br />
            </p>
          `
        : "";
    };
  },
  PHONE: function() {
    return function() {
      if (!data.PHONE_NUMBER) return "";
      const parsed = parsePhoneNumberFromString(data.PHONE_NUMBER);
      const uri = parsed && parsed.getURI();

      if (!parsed || !uri) {
        return "";
      }
      return `
        <p
          dir="ltr"
          style="line-height:1.38;margin-top:0pt;margin-bottom:0pt">
          <span style="font-size:8.5pt;font-family:Helvetica Neue,sans-serif;color:rgb(129,141,153);background-color:rgb(255,255,255);font-weight:600;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline">
            Tel
          </span>
          <a
            href="${uri}"
            style="text-decoration:none">
            <span style="font-size:8.5pt;font-family:Helvetica Neue,sans-serif;color:rgb(64,156,209);background-color:rgb(255,255,255);font-weight:400;font-style:normal;font-variant:normal;text-decoration:underline;vertical-align:baseline;white-space:pre-wrap">${data.PHONE_NUMBER}</span>
          </a>
        </p>
      `;
    };
  }
};

export default data;
