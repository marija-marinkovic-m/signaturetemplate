const form = `
  <div class="pure-form pure-form pure-form-aligned">
    <form>
      <fieldset>
        <div class="pure-control-group">
          <label for="your_name" class="pure-u-2-5">Name</label>
          <input name="your_name" type="text" placeholder="Enter your name" value="{{YOUR_NAME}}" class="pure-u-3-5">
        </div>
        <div class="pure-control-group">
          <label for="your_email" class="pure-u-2-5">Email</label>
          <input name="your_email" type="text" value="{{YOUR_EMAIL}}" class="pure-u-3-5">
        </div>
        <div class="pure-control-group">
          <label for="your_title" class="pure-u-2-5">Job Title</label>
          <input name="your_title" type="text" value="{{YOUR_TITLE}}" class="pure-u-3-5">
        </div>
        <div class="pure-control-group">
          <label for="phone_number" class="pure-u-2-5">Phone Number <span style="color: rgb(129,141,153)">(optional)</></label>

          <div class="pure-u-3-5">
          <input placeholder="+123 (456) 789-5850" name="phone_number" type="text" value="{{PHONE_NUMBER}}" class="pure-input-1">

            <span id="phone_number-feedback" class="warning"></span>
          </div>
        </div>

        <br />

        <div class="pure-control-group">
          <label for="banner_src" class="pure-u-2-5">Choose The Banner</label>
          <select name="banner_src" class="pure-u-3-5">
            {{#banners}}
              <option value="{{src}}" {{selected}}>{{label}}</option>
            {{/banners}}
          </select>
        </div>
      </fieldset>
    </form>
    <br />

    <fieldset>
      <div class="pure-control-group">
        <label class="pure-u-2-5">Instructions</label>
        <div class="pure-u-3-5">
          <div class="pure-menu pure-menu">
            <ul class="accordion pure-menu-list">
                <li class="pure-menu-item">
                    <input id="gmail-i" name="gmail-i" type="radio" checked="">
                    <label for="gmail-i" class="pure-menu-link">GMAIL</label>
                    <div class="content">
                      <article>
                        <ol>
                          <li><a href="#" class="btn" data-clipboard-target="#preview-el">Copy your finished signature</a> and paste it into the WYSIWYG editor</li>
                          <li>Scroll down to the bottom of the Gmail settings page to click 'Save Changes'. </li>
                          <li>Then send yourself an email, make sure your signature shows and check all the links</li>
                          <li>As a final step, send an email to a colleague and ask him/her to double check the layout and the link</li>
                        </ol>

                        <button class="btn pure-button button-warning" data-clipboard-target="#preview-el">Copy your finished signature</button>
                      </article>
                    </div>
                </li>
                <li class="pure-menu-item">
                    <input id="hubspot-i" name="gmail-i" type="radio">
                    <label for="hubspot-i" class="pure-menu-link">HubSpot</label>
                    <div class="content">
                      <article>
                        <ol>
                          <li>In your HubSpot account, click your account name in the top right corner, then click "Profile & Preferences".</li>
                          <li>Scroll down to the <i>Signature</i> section and click "Edit signature" button.</li>
                          <li>Copy the code from the text box down below.</li>
                          <li>In the Simple editor, click HTML and paste the HTML of your signature.</li>
                          <li>Click "Save".</li>
                        </ol>
                  
                        <button class="btn pure-button" data-clipboard-target="#source-code">
                          Click to copy the code
                        </button>
                        <fieldset id="code-area"></fieldset>
                      </article>
                    </div>
                </li>
            </ul>
          </div>
        </div>
      </div>
    </fieldset>
  </div>
`;

export default form;
