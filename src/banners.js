import { CDN_BANNERS_URL } from './constants';

const banners = [
  { src: "", label: "No Banner" },
  {
    label: 'Brisbane White 01',
    src: `${CDN_BANNERS_URL}/Banner-footer-Brisbane-01-white.png`,
  },
  {
    label: 'Brisbane Blue 01',
    src: `${CDN_BANNERS_URL}/Banner-footer-Brisbane-01-blue.png`
  },
  {
    label: 'Toronto White 01',
    src: `${CDN_BANNERS_URL}/Banner-footer-Toronto-01-white.png`
  },
  {
    label: 'Toronto Blue 01',
    src: `${CDN_BANNERS_URL}/Banner-footer-Toronto-01-blue.png`
  },
  {
    label: 'Vienna White 01',
    src: `${CDN_BANNERS_URL}/Banner-footer-Vienna-01-white.png`
  },
  {
    label: 'Vienna Blue 01',
    src: `${CDN_BANNERS_URL}/Banner-footer-Vienna-01-blue.png`
  },
  {
    src: `${CDN_BANNERS_URL}/Banner-footer-generic-01-blue.png`,
    label: "Blue 01" },
  {
    src: `${CDN_BANNERS_URL}/Banner-footer-generic-01-white.png`,
    label: "White 01"
  },
  {
    src: `${CDN_BANNERS_URL}/Banner-footer-generic-02-blue.png`,
    label: "Blue 02"
  },
  {
    src: `${CDN_BANNERS_URL}/Banner-footer-generic-02-white.png`,
    label: "White 02"
  },
  {
    src: `${CDN_BANNERS_URL}/Banner-footer-generic-03-blue.png`,
    label: "Blue 03"
  },
  {
    src: `${CDN_BANNERS_URL}/Banner-footer-generic-03-white.png`,
    label: "White 03"
  },
  {
    src: `${CDN_BANNERS_URL}/Banner-footer-generic-04-blue.png`,
    label: "Blue 04"
  },
  {
    src: `${CDN_BANNERS_URL}/Banner-footer-generic-04-white.png`,
    label: "White 04"
  },
  {
    src: `${CDN_BANNERS_URL}/Banner-footer-life-at-tr-01.png`,
    label: "Life at TR 01"
  },
  {
    src: `${CDN_BANNERS_URL}/Banner-footer-life-at-tr-02.png`,
    label: "Life at TR 02"
  },
  {
    src: `${CDN_BANNERS_URL}/Banner-footer-life-at-tr-03.png`,
    label: "Life at TR 03"
  }
];

export default banners;
