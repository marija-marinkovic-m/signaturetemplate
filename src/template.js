import { S3_CDN_URL } from "./constants";

const template = `
  <table
    cellpadding="0"
    cellspacing="0"
    class="sc-jDwBTQ dWtMUn"
    style="vertical-align: -webkit-baseline-middle; font-size: medium; font-family: Arial;"
  >
    <tbody>
      <tr>
        <td>
          <div
            dir="ltr"
            class="m_-2837310973056925547gmail_signature"
            data-smartmail="gmail_signature"
          >
            <div dir="ltr">
              <div>
                <div dir="ltr">
                  <div>
                    <div dir="ltr">
                      <span>
                        <p
                          dir="ltr"
                          style="line-height:1.38;margin-top:0pt;margin-bottom:0pt"
                        >
                          <span
                            style="font-size:10.5pt;font-family:Helvetica Neue,sans-serif;color:rgb(51,51,51);background-color:rgb(255,255,255);font-weight:700;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre-wrap"
                            >{{YOUR_NAME}}</span
                          >
                        </p>
                        <p
                          dir="ltr"
                          style="line-height:1.38;margin-top:0pt;margin-bottom:0pt"
                        >
                          <span
                            style="font-size:10.5pt;font-family:Helvetica Neue,sans-serif;color:rgb(129,141,153);background-color:rgb(255,255,255);font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre-wrap"
                            >{{YOUR_TITLE}}</span
                          ><span
                            style="font-size:8.5pt;font-family:Helvetica Neue,sans-serif;color:rgb(153,153,153);background-color:rgb(255,255,255);font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre-wrap"
                            ><br /></span
                          >
                          <span style="font-size:8.5pt;font-family:Helvetica Neue,sans-serif;color:rgb(129,141,153);background-color:rgb(255,255,255);font-weight:600;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline">
                Email
              </span>
                          <a
                            href="mailto:{{YOUR_EMAIL}}"
                            style="text-decoration:none"
                            target="_blank"
                            ><span
                              style="font-size:8.5pt;font-family:Helvetica Neue,sans-serif;color:rgb(64,156,209);background-color:rgb(255,255,255);font-weight:400;font-style:normal;font-variant:normal;text-decoration:underline;vertical-align:baseline;white-space:pre-wrap"
                              >{{YOUR_EMAIL}}</span
                            ></a
                          >
                        </p>
                        {{#PHONE}}{{PHONE_NUMBER}}{{/PHONE}}
                        <p
                          dir="ltr"
                          style="line-height:1.38;margin-top:0pt;margin-bottom:0pt"
                        >
                          <b style="font-weight:normal"><br /></b>
                        </p>
                        <p
                          dir="ltr"
                          style="line-height:1.38;margin-top:0pt;margin-bottom:0pt"
                        >
                          <a
                            href="https://www.tourradar.com/"
                            style="text-decoration:none"
                            target="_blank"
                            data-saferedirecturl="https://www.google.com/url?q=https://www.tourradar.com/&amp;source=gmail&amp;ust=1568126779647000&amp;usg=AFQjCNHj6zONvr80FzLvspO--aJo7oqInw"
                            ><span
                              style="font-size:8.5pt;font-family:Helvetica Neue,sans-serif;color:rgb(17,85,204);background-color:rgb(255,255,255);font-weight:400;font-style:normal;font-variant:normal;text-decoration:underline;vertical-align:baseline;"
                              ><img
                                src="${S3_CDN_URL}/logo.png"
                                width="183"
                                height="30"
                                style="border:none"
                                class="CToWUd"/></span
                          ></a>
                        </p>
                        <p
                          dir="ltr"
                          style="line-height:1.38;margin-top:0pt;margin-bottom:0pt"
                        >
                          <b style="font-weight:normal"><br /></b>
                        </p>
                        <p
                          dir="ltr"
                          style="line-height:1.38;margin-top:0pt;margin-bottom:0pt"
                        >
                          <a
                            href="https://www.facebook.com/TourRadar/"
                            style="text-decoration:none"
                            target="_blank"
                            ><span
                              style="font-size:8.5pt;font-family:Helvetica Neue,sans-serif;color:rgb(17,85,204);background-color:rgb(255,255,255);font-weight:400;font-style:normal;font-variant:normal;text-decoration:underline;vertical-align:baseline;"
                              ><img
                                src="${S3_CDN_URL}/fb-ico.png"
                                width="32"
                                height="32"
                                style="border:none"
                                class="CToWUd"/></span></a
                          ><span
                            style="font-size:9.5pt;font-family:Arial;color:rgb(34,34,34);background-color:rgb(255,255,255);font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;"
                          >
                          </span
                          ><a
                            href="https://www.instagram.com/tourradar/"
                            style="text-decoration:none"
                            target="_blank"
                            ><span
                              style="font-size:9.5pt;font-family:Arial;color:rgb(17,85,204);background-color:rgb(255,255,255);font-weight:400;font-style:normal;font-variant:normal;text-decoration:underline;vertical-align:baseline;"
                              ><img
                                src="${S3_CDN_URL}/insta-ico.png"
                                width="32"
                                height="32"
                                style="border:none"
                                class="CToWUd"/></span></a
                          ><span
                            style="font-size:9.5pt;font-family:Arial;color:rgb(34,34,34);background-color:rgb(255,255,255);font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;"
                          >
                          </span
                          ><a
                            href="http://youtube.com/tourradar"
                            style="text-decoration:none"
                            target="_blank"
                            ><span
                              style="font-size:9.5pt;font-family:Arial;color:rgb(17,85,204);background-color:rgb(255,255,255);font-weight:400;font-style:normal;font-variant:normal;text-decoration:underline;vertical-align:baseline;"
                              ><img
                                src="${S3_CDN_URL}/yt-ico.png"
                                width="32"
                                height="32"
                                style="border:none"
                                class="CToWUd"/></span></a
                          ><span
                            style="font-size:9.5pt;font-family:Arial;color:rgb(34,34,34);background-color:rgb(255,255,255);font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;"
                          >
                          </span
                          ><a
                            href="https://twitter.com/tourradar"
                            style="text-decoration:none"
                            target="_blank"
                            ><span
                              style="font-size:9.5pt;font-family:Arial;color:rgb(17,85,204);background-color:rgb(255,255,255);font-weight:400;font-style:normal;font-variant:normal;text-decoration:underline;vertical-align:baseline;"
                              ><img
                                src="${S3_CDN_URL}/tw-ico.png"
                                width="32"
                                height="32"
                                style="border:none"
                                class="CToWUd"/></span></a
                          ><span
                            style="font-size:9.5pt;font-family:Arial;color:rgb(34,34,34);background-color:rgb(255,255,255);font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;"
                          >
                          </span
                          ><span
                            style="text-decoration:underline;font-size:9.5pt;font-family:Arial;color:rgb(17,85,204);background-color:rgb(255,255,255);font-weight:400;font-style:normal;font-variant:normal;vertical-align:baseline;white-space:pre-wrap"
                            ><a
                              href="https://www.pinterest.at/tourradar/"
                              style="text-decoration:none"
                              target="_blank"
                              ><img
                                src="${S3_CDN_URL}/pinterest-ico.png"
                                width="32"
                                height="32"
                                style="border:none"
                                class="CToWUd"/></a
                          ></span>
                        </p>{{#BANNER}}{{BANNER_SRC}}{{/BANNER}}
                        <p
                          dir="ltr"
                          style="line-height:1.38;margin-top:0pt;margin-bottom:0pt"
                        ></p>
                      </span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </td>
      </tr>
    </tbody>
  </table>
`;

export default template;
