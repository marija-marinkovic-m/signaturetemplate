import "./styles.css";

import { render } from "mustache";
import Clipboard from "clipboard";
import { parsePhoneNumberFromString } from "libphonenumber-js";

import template from "./template";
import form from "./form";
import banners from "./banners";
import data from "./data";

const renderSignature = (div, source, template = template, data = data) => {
  const html = render(template, data);
  div.innerHTML = html;
  source.innerHTML = html;
};

const source = document.createElement("textarea");
source.setAttribute("id", "source-code");
source.setAttribute("onclick", "this.focus();this.select();");
source.setAttribute("readonly", "readonly");

const theForm = document.createElement("div");
theForm.setAttribute("class", "pure-u-1 pure-u-lg-1-2 pure-u-xl-2-5");
theForm.innerHTML = render(form, {
  ...data,
  banners,
  selected: function() {
    return this.src === data.BANNER_SRC ? "selected" : "";
  }
});

const previewSection = document.createElement("div");
previewSection.setAttribute("class", "pure-u-1 pure-u-lg-1-2 pure-u-xl-3-5");

const div = document.createElement("div");
div.setAttribute("id", "preview-el");

document.body.appendChild(theForm);
document.body.appendChild(previewSection);

previewSection.append(div);

renderSignature(div, source, template, data);

document.getElementById("code-area").appendChild(source);

// event listeners
const inputs = document.querySelectorAll("input");
inputs.forEach(function(input) {
  input.addEventListener("input", function({ srcElement: { name, value } }) {
    data[name.toUpperCase()] = value;

    if (name === "phone_number") {
      const fElem = document.getElementById("phone_number-feedback");
      if (fElem) {
        const parsed = parsePhoneNumberFromString(value);
        fElem.innerText =
          value === "" || (parsed && parsed.getURI())
            ? ""
            : "Please, enter valid phone number";
      }
    }
    renderSignature(div, source, template, data);
  });
});

const bannerSelects = document.getElementsByName("banner_src");
bannerSelects.forEach(function(select) {
  select.addEventListener("change", function(e) {
    const src = e.srcElement.value;
    data.BANNER_SRC = src !== "" ? src : null;
    renderSignature(div, source, template, data);
  });
});

const clipboard = new Clipboard(".btn");
clipboard.on("success", function(e) {
  alert("Signature copied to the clipboard");
});
